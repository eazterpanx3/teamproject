/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import databasemovie.Movie;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Home
 */
public class moviemodel extends AbstractTableModel {
    ArrayList<Movie> movieList = new ArrayList<Movie>();
    String[] columnMovie = {"movie id","movie name","movie type","rent"};

    @Override
    public int getRowCount() {
        return movieList.size();
          }

    @Override
    public int getColumnCount() {
        return columnMovie.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Movie movie = movieList.get(rowIndex);
        switch(columnIndex){
            case 0: return movie.getMovieId();
                 case 1: return movie.getMoviename();
                      case 2: return movie.getMovietype();
                           case 3: return movie.getRent();
                                case 4: return movie.getTypeid()==0? "Normal":"Vip";
        }
        return "";
    }
     public void setData(ArrayList<Movie>movieList){
        this.movieList = movieList;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnMovie [column];
    
    
    }   
    
}
