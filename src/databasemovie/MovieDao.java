/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemovie;


import databasemovie.DatebaseMovie;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import librarayprojectmovie.LibrarayProjectMovie;

/**
 *
 * @author witha
 */
public class MovieDao {
    public static boolean insert(Movie movie){
        Connection conn = DatebaseMovie.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO movie (\n" +
"                 moviename,\n" +
"                 rate,\n" +
"                 movietype,\n" +
"                 rent,\n" +
"                 typeid\n" +
"              )\n" +
"                  VALUES (\n" +
"               '%s',\n" +
"               '%s',\n" +
"               '%s',\n" +
"               '%s',\n" +
"               %d\n" +
"                ); " ;
            System.out.println(String.format(sql, movie.getMoviename(),movie.getRate(),movie.getMovietype(),movie.getRent(),movie.getTypeid() ));
            stm.execute(String.format(sql, movie.getMoviename(),movie.getRate(),movie.getMovietype(),movie.getRent(),movie.getTypeid() ));
            
            DatebaseMovie.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MovieDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        DatebaseMovie.close();
        return true;
    }
    public static boolean update(Movie movie){
        String sql = "UPDATE movie SET\n" +
"        moviename = '%s',\n" +
"       rate = '%s',\n" +
"       movietype = '%s',\n" +
"       rent = '%s',\n" +
"       typeid = %d\n" +
" WHERE movieId = %d ; ";
        Connection conn = DatebaseMovie.connect();
        Statement stm;
        try {
            stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql ,movie.getMoviename(),movie.getRate()
                    ,movie.getMovietype(),movie.getRent(),movie.getTypeid(), movie.getMovieId() ));
            DatebaseMovie.close();
            return ret;
        
        } catch (SQLException ex) {
            Logger.getLogger(MovieDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DatebaseMovie.close();
        return false;
    }
    public static boolean delete(Movie movie){
        String sql = "DELETE FROM movie  WHERE movieId = %d ; ";
        Connection conn = DatebaseMovie.connect();
       
        try {
             Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, movie.getMovieId()));
            DatebaseMovie.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(MovieDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        DatebaseMovie.close();
        return true;
    }
    public static ArrayList<Movie> getMovies(){
        ArrayList<Movie> list = new ArrayList();
        Connection conn = DatebaseMovie.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT movieId,\n" +
"       moviename,\n" +
"       rate,\n" +
"       movietype,\n" +
"       rent,\n" +
"       typeid\n" +
"  FROM movie";
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                 System.out.println(rs.getInt("movieId") + " " + rs.getString("moviename"));
                 Movie movie = toObject(rs);
                 list.add(movie);
                 
        
                  
            }
            DatebaseMovie.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(LibrarayProjectMovie.class.getName()).log(Level.SEVERE, null, ex);
        }
        DatebaseMovie.close();
        return null;
    }

    private static Movie toObject(ResultSet rs) throws SQLException {
        Movie movie;
        movie =  new Movie();
        movie.setMovieId(rs.getInt("movieId"));
        movie.setMoviename(rs.getString("moviename"));
        movie.setMovietype(rs.getString("movietype"));
        movie.setRent(rs.getString("rent"));
        movie.setRate(rs.getString("rate"));
        movie.setTypeid(rs.getInt("typeid"));
        return movie;
    }

    
    public static Movie getMovie(int movieId){
        String sql = "SELECT * FROM movie WHERE movieId = %d";
        Connection conn =  DatebaseMovie.connect();
       
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, movieId));
            if(rs.next()){
                Movie movie = toObject(rs);
                DatebaseMovie.close();
                return movie;
            }
        } catch (SQLException ex) {
            Logger.getLogger(MovieDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        DatebaseMovie.close();
        return null;
    }
    public static void save(Movie movie){
        if(movie.getMovieId()<0){
            insert(movie);
        }else{
            update(movie);
        }
    }
}
