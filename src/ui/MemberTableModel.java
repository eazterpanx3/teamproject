/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;
import database.Member;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Admin
 */
public class MemberTableModel extends AbstractTableModel{
    static ArrayList<Member> memberList = new ArrayList<Member>();
    String[] columnName = {"Card_id","Member_id","Member_type","Member_name","Member_lastname","Address","Tel_number"};
    
    
    

    @Override
    public int getRowCount() {
       return memberList.size(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getColumnCount() {
        return columnName.length; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Member mem = memberList.get(rowIndex);
        switch(columnIndex){
            case 0:return mem.getCard_ID();
            case 1:return mem.getMember_id();
            case 2:return mem.getMember_type()==0? "NORMAL":"VIP";
            case 3:return mem.getMember_name();
            case 4:return mem.getMember_lastname();
            case 5:return mem.getAddress();
            case 6:return mem.getTel_number();
    }
        return "";
   }
    public  void setData(ArrayList<Member>usrList){
        this.memberList = usrList;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
       return columnName[column];
    }

    
}