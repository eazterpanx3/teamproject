package ui;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import registdatabase.User_rs;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author witha
 */
public class RegistTablemodle extends AbstractTableModel{
    ArrayList<User_rs> userList = new ArrayList<User_rs>();
    String[] ColumnNames = {"customer id","Tel Number","name","surname","Card_ID"};
    @Override
    public int getRowCount() {
      return userList.size();
    }

    @Override
    public int getColumnCount() {
        return ColumnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
       User_rs user_rs = userList.get(rowIndex);
       switch(columnIndex){
           case 0: return user_rs.getRe_ID();
            case 1: return user_rs.getId_card();
                 case 2: return user_rs.getName();
                      case 3: return user_rs.getSurname();
                          case 4: return user_rs.getTel();
                             case 5: return user_rs.getType()==0? "Normal":"Vip";
       }
       return "";
    }
    public void setData(ArrayList<User_rs>userList){
        this.userList = userList;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
       return ColumnNames[column];
    }
    
}