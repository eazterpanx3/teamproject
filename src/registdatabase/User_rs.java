/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package registdatabase;

/**
 *
 * @author witha
 */
public class User_rs {
    int re_ID;
    String Id_card;
    String name;
    String surname;
    String Tel;
    int type;
    
   

    @Override
    public String toString() {
        return "User_rs{" + "re_ID=" + re_ID + ", Id_card=" + Id_card + ", name=" + name + ", surname=" + surname + ", Tel=" + Tel + ", type=" + type + '}';
    }

    public User_rs() {
        this.re_ID = -1;
    }
   
    public int getRe_ID() {
        return re_ID;
    }

    public void setRe_ID(int re_ID) {
        this.re_ID = re_ID;
    }

    public String getId_card() {
        return Id_card;
    }

    public void setId_card(String Id_card) {
        this.Id_card = Id_card;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getTel() {
        return Tel;
    }

    public void setTel(String Tel) {
        this.Tel = Tel;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
    
    
}
