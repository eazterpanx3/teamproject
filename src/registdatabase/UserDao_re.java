/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package registdatabase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import librarayproject.LibrarayProject;

/**
 *
 * @author witha
 */
public class UserDao_re {
    public static boolean insert(User_rs user){
        Connection conn = Database_re.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO regist (\n" +
"                       type,\n" +
"                       Tel,\n" +
"                       surname,\n" +
"                       name,\n" +
"                       Id_card,\n" +
"                       re_ID\n" +
"                   )\n" +
"                   VALUES (\n" +
"                       %d,\n" +
"                       '%s',\n" +
"                       '%s',\n" +
"                       '%s',\n" +
"                       '%s',\n" +
"                       %d\n" +
"                   );";
            stm.execute(String.format(sql, user.getRe_ID(),user.getId_card(),user.getName(),user.getSurname(),user.getTel(),user.getType()));
        } catch (SQLException ex) {
            Logger.getLogger(UserDao_re.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database_re.close();
        return true;
    }
     public static boolean update(User_rs user){
                    String sql = "UPDATE regist\n" +
                                "   SET re_ID = '%d',\n" +
                                "       Id_card = '%s',\n" +
                                "       name = '%s',\n" +
                                "       surname = '%s',\n" +
                                "       Tel = '%s',\n" +
                                "       type = '%d'";
         Connection conn = Database_re.connect();
         Statement stm;
        try {
            stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql,
                 user.getRe_ID(),
                 user.getId_card(),
                 user.getName(),
                 user.getSurname(),
                 user.getTel(),
                 user.getType()
                 ));
            Database_re.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao_re.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         Database_re.close();
        return false;
    }
      public static boolean delete(User_rs user){
          String sql = "DELETE FROM regist WHERE re_ID = %d";
          Connection conn = Database_re.connect();
         
        try {
           Statement stm = conn.createStatement();
           boolean ret = stm.execute(String.format(sql, user.getRe_ID()));
            Database_re.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao_re.class.getName()).log(Level.SEVERE, null, ex);
        }
         
          Database_re.close();
        return true;
     }
       public static  ArrayList<User_rs> getUser_rss(){
           ArrayList<User_rs> list = new ArrayList();
        Connection conn = Database_re.connect();
        try{
            Statement stm = conn.createStatement();
            String sql = "SELECT re_ID,\n" +
                            "       Id_card,\n" +
                            "       name,\n" +
                            "       surname,\n" +
                            "       Tel,\n" +
                            "       type\n" +
                            "  FROM regist";
             ResultSet rs = stm.executeQuery(sql);
             while(rs.next()){
            System.out.println(rs.getInt("re_ID") + " " + rs.getString("Id_card"));
            User_rs user = ToObject(rs);
            list.add(user);
            
        }
             Database_re.close();
             return list;
        } catch (SQLException ex) {
            Logger.getLogger(LibrarayProject.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        
        Database_re.close();
        return null;
     }

    private static User_rs ToObject(ResultSet rs) throws SQLException {
        User_rs user;
        user = new User_rs ();
        user.setRe_ID(rs.getInt("re_id"));
        user.setId_card(rs.getString("Id_card"));
        user.setName(rs.getString("name"));
        user.setSurname(rs.getString("surname"));
        user.setTel(rs.getString("Tel"));
        user.setType(rs.getInt("type"));
        return user ;
    }
       public static   User_rs getUser_rs(int Re_ID){
           String sql = "SELECT * FROM user WHERE userId = %d" ;
           Connection conn =  Database_re.connect();
           
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql,Re_ID));
            if(rs.next()){
                User_rs user = ToObject(rs);
                Database_re.close();
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(User_rs.class.getName()).log(Level.SEVERE, null, ex);
        }
           Database_re.close();
           return null;
       }
       
       public static void save(User_rs user){
           
           if(user.getRe_ID()<0){
               insert(user);
               
           }else{
               update(user);
           }
       }
}
