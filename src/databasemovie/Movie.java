/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasemovie;

/**
 *
 * @author witha
 */
public class Movie {
   int      movieId;
   String   moviename;
   String   rate;
   String   movietype;
   String   rent;
   int      typeid;

    public Movie() {
        this.movieId = -1;
    }

    public Movie(int movieId, String moviename, String rate, String movietype, String rent, int typeid) {
        this.movieId = movieId;
        this.moviename = moviename;
        this.rate = rate;
        this.movietype = movietype;
        this.rent = rent;
        this.typeid = typeid;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public String getMoviename() {
        return moviename;
    }

    public void setMoviename(String moviename) {
        this.moviename = moviename;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getMovietype() {
        return movietype;
    }

    public void setMovietype(String movietype) {
        this.movietype = movietype;
    }

    public String getRent() {
        return rent;
    }

    public void setRent(String rent) {
        this.rent = rent;
    }

    public int getTypeid() {
        return typeid;
    }

    public void setTypeid(int typeid) {
        this.typeid = typeid;
    }

    @Override
    public String toString() {
        return "Movie{" + "movieId=" + movieId + ", moviename=" + moviename + ", rate=" + rate + ", movietype=" + movietype + ", rent=" + rent + ", typeid=" + typeid + '}';
    }
   
}
