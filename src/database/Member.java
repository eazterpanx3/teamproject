/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author Admi
 */
public class Member {
     String     Card_ID;
     int   Member_id;
     int     Member_type;
     String   Member_name;
     String   Member_lastname;
     String     Address;
     String Tel_number;
   

    public Member() {
        
    }

    public Member(String Card_ID, int Member_id, int Member_type, String Member_name, String Member_lastname, String Address, String Tel_number) {
        this.Card_ID = Card_ID;
        this.Member_id = Member_id;
        this.Member_type = Member_type;
        this.Member_name = Member_name;
        this.Member_lastname = Member_lastname;
        this.Address = Address;
        this.Tel_number = Tel_number;
    }

    public String getCard_ID() {
        return Card_ID;
    }

    public void setCard_ID(String Card_ID) {
        this.Card_ID = Card_ID;
    }

    public int getMember_id() {
        return Member_id;
    }

    public void setMember_id(int Member_id) {
        this.Member_id = Member_id;
    }

    public int getMember_type() {
        return Member_type;
    }

    public void setMember_type(int Member_type) {
        this.Member_type = Member_type;
    }

    public String getMember_name() {
        return Member_name;
    }

    public void setMember_name(String Member_name) {
        this.Member_name = Member_name;
    }

    public String getMember_lastname() {
        return Member_lastname;
    }

    public void setMember_lastname(String Member_lastname) {
        this.Member_lastname = Member_lastname;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getTel_number() {
        return Tel_number;
    }

    public void setTel_number(String Tel_number) {
        this.Tel_number = Tel_number;
    }

    @Override
    public String toString() {
        return "Member{" + "Card_ID=" + Card_ID + ", Member_id=" + Member_id + ", Member_type=" + Member_type + ", Member_name=" + Member_name + ", Member_lastname=" + Member_lastname + ", Address=" + Address + ", Tel_number=" + Tel_number + '}';
    }

    
     
     

}